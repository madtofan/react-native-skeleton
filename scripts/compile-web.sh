echo "### (STEP 1/5) Downloading dependencies to compile ###"
yarn

echo "### (STEP 2/5) Compiling into web-build folder ###"
expo build:web

echo "### (STEP 3/5) Clearing previous container ###"
docker stop web
docker rm web

echo "### (STEP 4/5) Building new container image ###"
docker build -t react-native-web .

echo "### (STEP 5/5) Running newly built image as a container ###"
docker run -dit --name web -p 80:80 react-native-web

docker ps