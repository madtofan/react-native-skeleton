---

## Introduction

This is an individual project for myself to train myself in creating a multi-platform interface with a single code.
Currently the project can be compiled for:

1. Desktop Application
2. Mobile Application (Android and iOS)
3. Web Application

Any update on this project will be updated on the trello page https://trello.com/b/lQ6fSUxe
The update will be done on personal time, hence the progress will be quite slow.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

---

## Installation

1. Make sure the items listed below are installed beforehand

   - NodeJS https://nodejs.org/en/download/
   - Yarn https://yarnpkg.com/getting-started/install

2. Run "yarn" on the root of the project to install all the dependencies

3. Run these commands for any selected environment

   yarn start - Starts the metro bundler on http://localhost:19002 only for mobile QR tunnelling
   yarn web - Starts the metro bundler on http://localhost:19002 and the webservice to http://localhost:19006
   yarn desktop - Spins up the electronJS to emulate the desktop executable

   sudo apt-get install libnss3-dev
   sudo apt-get install libgtk2.0-0
   sudo apt install libgtk-3-0
   sudo apt-get install libxss

   yarn build-web - Compiles the project into a container and runs as a docker image running a webserver on http://localhost (Requires Docker and ability to run shell script)

---

## Usage

1. ./assets folder retains all the images and other assets related to the project
2. ./src folder is where all the source code resides aside from App.tsx
3. ./src/routes folder is used to define the routing of the application
4. ./src/layouts pre-made layouts will be stored here to be used in the routes containers
5. ./src/utils mainly used for shared functional variables
6. ./src/components mainly used for shared component variables
7. ./src/stores is to use the mobx function of creating a global variable that can be used anywhere in the projects (Theme color and Responsive is not using mobx stores)
8. ./src/constants hold any constants that is used anywhere in the project

---