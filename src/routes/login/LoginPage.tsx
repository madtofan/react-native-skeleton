import * as React from "react";
import { useState } from "react";
import { Button, Image, StyleSheet, TextInput, View } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { CheckBox } from "src/components/checkbox/Checkbox";
import { ROOT_PATH } from "src/constants/route-path";
import { CenterContainerLayout } from "src/layouts/center-container/CenterContainerLayout";
import { GenericStoreContext } from "src/stores/GenericStore";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props extends RouteComponentProps {}

const LoginPageWithoutRouter: React.FC<Props> = ({ history }) => {
  const themeColor = getCurrentThemeColor();
  const styles = StyleSheet.create({
    container: {
      alignSelf: "center",
      alignItems: "center",
    },
    logo: {
      width: 140,
      height: 111,
      marginTop: 10,
    },
    errorView: {
      height: 20,
    },
    errorText: {
      color: "red",
    },
    inputContainer: {
      borderWidth: StyleSheet.hairlineWidth,
      borderRadius: 3,
      width: "90%",
      borderColor: themeColor.border,
      flexDirection: "row",
      height: 30,
      margin: 5,
    },
    checkboxView: {
      margin: 5,
      alignSelf: "flex-start",
    },
    inputLogo: {
      height: "100%",
      backgroundColor: themeColor.main,
      borderRightWidth: StyleSheet.hairlineWidth,
      borderColor: themeColor.border,
      width: 30,
      justifyContent: "center",
      alignItems: "center",
      opacity: 0.5,
    },
    inputText: {
      padding: 5,
      height: "100%",
      width: "100%",
    },
    loginDetails: {
      alignContent: "flex-start",
      margin: 5,
    },
  });

  const store = React.useContext(GenericStoreContext);
  const [showPassword, setshowPassword] = useState(false);
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");

  const logIn = async () => {
    store.loginStore.details = {
      loggedIn: true,
      token: "asd-123",
    };
    history.push(ROOT_PATH);
  };

  return (
    <CenterContainerLayout>
      <View style={styles.container}>
        <Image
          source={require("assets/images/logo.jpeg")}
          style={styles.logo}
        />
        <View style={styles.loginDetails}>
          <View style={styles.inputContainer}>
            <View style={styles.inputLogo}>
              <MaterialIcons
                size={25}
                color={themeColor.border}
                name="person"
              />
            </View>
            <TextInput
              placeholder="username"
              onChangeText={(text) => setusername(text)}
              value={username}
              style={styles.inputText}
            />
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputLogo}>
              <MaterialIcons size={20} color={themeColor.border} name="lock" />
            </View>
            <TextInput
              placeholder="password"
              secureTextEntry={!showPassword}
              onChangeText={(text) => setpassword(text)}
              value={password}
              style={styles.inputText}
              onSubmitEditing={async () => {
                await logIn();
              }}
            />
          </View>
          <CheckBox
            selected={showPassword}
            onPress={() => setshowPassword(!showPassword)}
            text="Show password"
            style={styles.checkboxView}
          />
        </View>
        <Button
          title="Sign in"
          color={themeColor.secondary}
          onPress={async () => {
            await logIn();
          }}
        />
      </View>
    </CenterContainerLayout>
  );
};

export const LoginPage = withRouter(LoginPageWithoutRouter);
