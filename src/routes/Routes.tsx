import { observer } from "mobx-react-lite";
import * as React from "react";
import { ProtectedRoute } from "src/components/protected-route/ProtectedRoute";
import { Redirect, Router, Switch } from "src/components/router/index";
import {
  HOME_PATH,
  INVITE_PATH,
  LOGIN_PATH,
  ROOT_PATH,
} from "src/constants/route-path";
import { HomePage } from "./home/HomePage";
import { InvitePage } from "./invite/InvitePage";
import { LoginPage } from "./login/LoginPage";

interface Props {}

export const Routes: React.FC<Props> = observer(() => {
  return (
    <Router>
      <Switch>
        <Redirect exact from={ROOT_PATH} to={HOME_PATH} />
        <ProtectedRoute
          path={HOME_PATH}
          component={HomePage}
          securedContent={true}
        />
        <ProtectedRoute
          path={INVITE_PATH}
          component={InvitePage}
          securedContent={false}
        />
        <ProtectedRoute
          path={LOGIN_PATH}
          component={LoginPage}
          securedContent={false}
        />
      </Switch>
    </Router>
  );
});
