import * as React from "react";
import { StyledText } from "src/components/styled-text/StyledText";
import { SinglePageLayout } from "src/layouts/single-page/SinglePageLayout";

interface Props {}

export const InvitePage: React.FC<Props> = () => {
  return (
    <SinglePageLayout>
      <StyledText>Test Invite</StyledText>
    </SinglePageLayout>
  );
};
