import * as React from "react";
import { useContext, useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { StyledText } from "src/components/styled-text/StyledText";
import { ScrollContentLayout } from "src/layouts/scroll-content/ScrollContentLayout";
import { GenericStoreContext } from "src/stores/GenericStore";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props {}

export const HomePage: React.FC<Props> = () => {
  const themeColor = getCurrentThemeColor();
  const styles = StyleSheet.create({
    container: {
      backgroundColor: themeColor.main,
      height: 49,
      width: "100%",
      alignItems: "center",
      flexDirection: "row",
      justifyContent: "space-between",
      paddingHorizontal: 20,
      borderBottomColor: themeColor.main,
      borderBottomWidth: 1,
    },
    headerGroupItems: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
    },
    headerUserImage: {
      width: 28,
      height: 28,
      borderRadius: 14,
    },
    headerTitle: {
      color: "white",
      fontFamily: "MontserratThin",
    },
  });

  const store = useContext(GenericStoreContext);
  const [profileImage, setProfileImage] = useState("");

  return (
    <ScrollContentLayout>
      <View style={styles.container}>
        <View style={styles.headerGroupItems}>
          <TouchableOpacity
            activeOpacity={0.5}
            style={{
              width: 60,
              justifyContent: "space-between",
              flexDirection: "row",
            }}
            onPress={() => {
              store.drawerStore.drawerOpen.drawerOpen = true;
            }}
          >
            {profileImage ? (
              <Image
                style={styles.headerUserImage}
                source={{ uri: profileImage }}
              />
            ) : (
              <MaterialIcons
                size={28}
                color={themeColor.bright}
                name="account-circle"
              />
            )}
            <View
              style={{
                alignItems: "center",
                width: 30,
                justifyContent: "center",
              }}
            >
              <MaterialIcons size={18} name="more-vert" color="white" />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.headerGroupItems}>
          <StyledText style={[styles.headerTitle, { fontSize: 20 }]}>
            {location.pathname}
          </StyledText>
        </View>
      </View>
    </ScrollContentLayout>
  );
};
