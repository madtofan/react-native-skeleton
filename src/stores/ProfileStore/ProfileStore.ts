import { observable } from "mobx";
import { persist } from "mobx-persist";
import { GenericStore } from "src/stores/GenericStore";

interface ProfileData {
  profileName: string;
  winCount: number;
  gamesPlayed: number;
}

export class ProfileStore {
  genericStore: GenericStore;

  constructor(genericStore: GenericStore) {
    this.genericStore = genericStore;
  }
  @persist("object") @observable profileData: ProfileData = {
    profileName: "",
    gamesPlayed: 0,
    winCount: 0,
  };
}
