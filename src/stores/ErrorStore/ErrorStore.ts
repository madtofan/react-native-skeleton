import { observable } from "mobx";
import { GenericStore } from "src/stores/GenericStore";

export interface errorData {
  status: number;
  message: string;
}

export class ErrorStore {
  genericStore: GenericStore;

  constructor(genericStore: GenericStore) {
    this.genericStore = genericStore;
  }
  @observable error: errorData = { message: "", status: 500 };
}
