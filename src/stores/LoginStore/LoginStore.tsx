import { observable } from "mobx";
import { persist } from "mobx-persist";
import { GenericStore } from "src/stores/GenericStore";

interface LoginDetails {
  loggedIn: boolean;
  token: string;
}

export class LoginStore {
  genericStore: GenericStore;

  constructor(genericStore: GenericStore) {
    this.genericStore = genericStore;
  }
  @persist("object") @observable details: LoginDetails = {
    loggedIn: false,
    token: "",
  };
}
