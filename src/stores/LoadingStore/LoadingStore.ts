import { observable } from "mobx";
import { GenericStore } from "src/stores/GenericStore";

export class LoadingStore {
  genericStore: GenericStore;

  constructor(genericStore: GenericStore) {
    this.genericStore = genericStore;
  }
  @observable loading = false;
}
