import { create } from "mobx-persist";
import { createContext } from "react";
import { AsyncStorage } from "react-native";
import { DrawerStore } from "./DrawerStore/DrawerStore";
import { ErrorStore } from "./ErrorStore/ErrorStore";
import { LoadingStore } from "./LoadingStore/LoadingStore";
import { LoginStore } from "./LoginStore/LoginStore";
import { ProfileStore } from "./ProfileStore/ProfileStore";
import { ThemeStore } from "./ThemeStore/ThemeStore";

const hydrate = create({
  storage: AsyncStorage,
  jsonify: true,
});

export class GenericStore {
  drawerStore = new DrawerStore(this);
  profileStore = new ProfileStore(this);
  loadingStore = new LoadingStore(this);
  errorStore = new ErrorStore(this);
  loginStore = new LoginStore(this);
  themeStore = new ThemeStore(this);

  constructor() {
    hydrate("profile", this.profileStore);
    hydrate("theme", this.themeStore);
    hydrate("login", this.loginStore);
  }
}

export const GenericStoreContext = createContext(new GenericStore());
