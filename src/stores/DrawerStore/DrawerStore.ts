import { observable } from "mobx";
import { GenericStore } from "src/stores/GenericStore";

interface DrawerData {
  drawerOpen: boolean;
  drawerLock: boolean;
}

export class DrawerStore {
  genericStore: GenericStore;

  constructor(genericStore: GenericStore) {
    this.genericStore = genericStore;
  }
  @observable drawerOpen: DrawerData = {
    drawerOpen: false,
    drawerLock: false,
  };
}
