import { observable } from "mobx";
import { persist } from "mobx-persist";
import { GenericStore } from "src/stores/GenericStore";
import { ColorType } from "src/utils/Color";

interface ThemeData {
  themeType: ColorType;
}

export class ThemeStore {
  genericStore: GenericStore;

  constructor(genericStore: GenericStore) {
    this.genericStore = genericStore;
  }
  @persist("object") @observable themeData: ThemeData = {
    themeType: "Basic",
  };
}
