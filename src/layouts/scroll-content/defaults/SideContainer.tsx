import * as React from "react";
import { useContext } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { StyledText } from "src/components/styled-text/StyledText";
import { ROOT_PATH } from "src/constants/route-path";
import { GenericStoreContext } from "src/stores/GenericStore";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props extends RouteComponentProps {}

const SideContainerWithoutRouter: React.FC<Props> = ({ history }) => {
  const styles = StyleSheet.create({
    container: {
      flexDirection: "column",
      backgroundColor: getCurrentThemeColor().secondary,
      height: "100%",
    },
  });

  const store = useContext(GenericStoreContext);

  const logOut = () => {
    store.loginStore.details = {
      loggedIn: false,
      token: "",
    };
    store.drawerStore.drawerOpen.drawerOpen = false;
    history.push(ROOT_PATH);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => logOut()}>
        <StyledText>Log Out</StyledText>
      </TouchableOpacity>
    </View>
  );
};

export const SideContainer = withRouter(SideContainerWithoutRouter);
