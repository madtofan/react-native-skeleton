import * as React from "react";
import { NavigationButton } from "src/components/navigation-button/NavigationButton";
import { HOME_PATH, INVITE_PATH, LOGIN_PATH } from "src/constants/route-path";

interface Props {}

export const NavigationContainer: React.FC<Props> = () => {
  return (
    <>
      <NavigationButton
        icon="insert-invitation"
        path={HOME_PATH}
        textButton="Home"
      />
      <NavigationButton
        icon="dashboard"
        path={INVITE_PATH}
        textButton="Dashboard"
      />
      <NavigationButton icon="dashboard" path={LOGIN_PATH} textButton="Game" />
    </>
  );
};
