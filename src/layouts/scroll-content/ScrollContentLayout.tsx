import { LinearGradient } from "expo-linear-gradient";
import { observer } from "mobx-react-lite";
import * as React from "react";
import { SafeAreaView, ScrollView, StyleSheet } from "react-native";
import Drawer from "react-native-drawer";
import { GLOBAL_STYLE } from "src/constants/styling";
import { GenericStoreContext } from "src/stores/GenericStore";
import {
  getCurrentThemeColor,
  mobileResponsive,
} from "src/utils/ContextProvider";
import { NavigationContainer } from "./defaults/NavigationContainer";
import { SideContainer } from "./defaults/SideContainer";

interface Props {
  sideContainer?: React.FC;
  navigationContainer?: React.FC;
}

export const ScrollContentLayout: React.FC<Props> = observer(
  ({
    children,
    sideContainer = <SideContainer />,
    navigationContainer = <NavigationContainer />,
  }) => {
    const themeColor = getCurrentThemeColor();
    const drawerStyles = {
      drawer: { shadowColor: "black", shadowOpacity: 0.8, shadowRadius: 3 },
      main: { paddingLeft: 3 },
    };
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: themeColor.main,
        height: "100%",
      },
      wrapper: {
        width: "100%",
        backgroundColor: themeColor.main,
      },
      navButtonsContainer: {
        backgroundColor: themeColor.main,
        height: 49,
        width: "100%",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
      },
    });
    const store = React.useContext(GenericStoreContext);
    const mobile = mobileResponsive();

    return (
      <Drawer
        open={mobile ? store.drawerStore.drawerOpen.drawerOpen : true}
        onClose={() => (store.drawerStore.drawerOpen.drawerOpen = false)}
        disabled={store.drawerStore.drawerOpen.drawerLock}
        type="overlay"
        content={sideContainer}
        tapToClose={mobile}
        openDrawerOffset={0.2}
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        styles={drawerStyles}
        captureGestures={mobile}
        tweenHandler={(ratio) => ({
          main: { opacity: (2 - ratio) / 2 },
        })}
      >
        <SafeAreaView style={styles.container}>
          <ScrollView
            style={styles.wrapper}
            bounces={true}
            pinchGestureEnabled={false}
          >
            {children}
          </ScrollView>
        </SafeAreaView>
        <LinearGradient
          locations={[0, 0.15, 0.15]}
          colors={[themeColor.main, themeColor.secondary, themeColor.secondary]}
          style={[styles.navButtonsContainer, GLOBAL_STYLE.shadow]}
        >
          {navigationContainer}
        </LinearGradient>
      </Drawer>
    );
  }
);
