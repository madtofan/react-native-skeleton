import * as React from "react";
import { StyleSheet, View } from "react-native";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { NavigationButton } from "src/components/navigation-button/NavigationButton";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props extends RouteComponentProps {}

const SinglePageLayoutWithoutRouter: React.FC<Props> = ({
  children,
  history,
}) => {
  const themeColor = getCurrentThemeColor();
  const styles = StyleSheet.create({
    container: {},
    headerGroupItems: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
      backgroundColor: themeColor.main,
      paddingBottom: 10,
    },
    closeButton: {
      width: 20,
      height: 20,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.headerGroupItems}>
        <View style={styles.closeButton}>
          <NavigationButton
            icon="close"
            onPress={() => {
              history.goBack();
            }}
          />
        </View>
      </View>
      {children}
    </View>
  );
};

export const SinglePageLayout = withRouter(SinglePageLayoutWithoutRouter);
