import * as React from "react";
import { ImageBackground, StyleProp, StyleSheet, View } from "react-native";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props {
  backroundImage?: string;
  containerStyle?: StyleProp<View> | Array<StyleProp<View>>;
}

export const CenterContainerLayout: React.FC<Props> = ({
  children,
  backroundImage = "assets/images/background-image.jpg",
}) => {
  const themeColor = getCurrentThemeColor();
  const styles = StyleSheet.create({
    container: {
      alignItems: "center",
      justifyContent: "center",
      width: "100%",
      height: "100%",
    },
    centerContainer: {
      backgroundColor: themeColor.main,
      padding: 6,
      flexDirection: "column",
      justifyContent: "space-around",
      alignItems: "center",
      borderRadius: 10,
      minHeight: 300,
      minWidth: 200,
    },
  });

  return (
    <ImageBackground
      source={require("assets/images/background-image.jpg")}
      style={styles.container}
    >
      <View style={styles.centerContainer}>{children}</View>
    </ImageBackground>
  );
};
