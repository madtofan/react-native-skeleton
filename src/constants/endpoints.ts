const HOST_IP = "localhost:8000";

export const LOGIN_ENDPOINT = `http://${HOST_IP}/oauth/token`;
export const LOGOUT_ENDPOINT = `http://${HOST_IP}/api/logout`;
