import moment, { Moment } from "moment";

export const ToDurationString = (timeToCompare: Moment) => {
  const duration = moment.duration(moment().diff(timeToCompare));
  if (duration.asYears() > 1) {
    return `${Math.round(duration.asYears())} years ago`;
  } else if (duration.asMonths() > 1) {
    return `${Math.round(duration.asMonths())} months ago`;
  } else if (duration.asWeeks() > 1) {
    return `${Math.round(duration.asWeeks())} weeks ago`;
  } else if (duration.asDays() > 1) {
    return `${Math.round(duration.asDays())} days ago`;
  } else if (duration.asHours() > 1) {
    return `${Math.round(duration.asHours())} hours ago`;
  } else {
    return `${Math.round(duration.asMonths())} minutes ago`;
  }
};
