export interface ColorScheme {
  colorType: ColorType;
  main: string;
  font: string;
  bright: string;
  secondary: string;
  border: string;
}

export type ColorType = "Basic" | "Dark" | "Light";

const BASIC_MODE: ColorScheme = {
  colorType: "Basic",
  main: "#3F2B80",
  font: "#B9A3FF",
  bright: "#FFFFFF",
  secondary: "#6646CC",
  border: "#5C5280",
};
const DARK_MODE: ColorScheme = {
  colorType: "Dark",
  main: "#3F2B80",
  font: "#B9A3FF",
  bright: "#7E57FF",
  secondary: "#6646CC",
  border: "#5C5280",
};
const LIGHT_MODE: ColorScheme = {
  colorType: "Light",
  main: "#3F2B80",
  font: "#B9A3FF",
  bright: "#7E57FF",
  secondary: "#6646CC",
  border: "#5C5280",
};

export const COLORS: ColorScheme[] = [BASIC_MODE, DARK_MODE, LIGHT_MODE];
