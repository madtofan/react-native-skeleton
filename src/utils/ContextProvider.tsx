import * as React from "react";
import { COLORS, ColorScheme, ColorType } from "src/utils/Color";

interface Context {
  mobile: boolean;
  colorScheme: ColorScheme;
  setColorType: (color: ColorType) => void;
}

const placeholderFunction = (color: ColorType) => {};

const emptyColorScheme: ColorScheme = {
  colorType: "Basic",
  main: "black",
  font: "black",
  bright: "black",
  secondary: "black",
  border: "black",
};

const generalContext = React.createContext<Context>({
  mobile: false,
  colorScheme: emptyColorScheme,
  setColorType: placeholderFunction,
});

interface Props {}

export const ContextProvider: React.FC<Props> = ({ children }) => {
  const breakpoint = 620;
  const [mobile, setMobile] = React.useState(window.innerWidth < breakpoint);
  const [colorType, setInternalColorType] = React.useState<ColorType>("Basic");
  const [colorScheme, setColorScheme] = React.useState<ColorScheme>(
    emptyColorScheme
  );

  const handleWindowResize = () => {
    setMobile(window.innerWidth < breakpoint);
  };

  React.useEffect(() => {
    window.addEventListener("resize", handleWindowResize);
    return () => window.removeEventListener("resize", handleWindowResize);
  }, []);

  React.useEffect(() => {
    setColorScheme(getColorScheme());
  }, [colorType]);

  const getColorScheme = (): ColorScheme => {
    const findResult = COLORS.find((color) => color.colorType === colorType);
    return findResult ? findResult : emptyColorScheme;
  };

  const setColorType = (color: ColorType) => {
    setInternalColorType(color);
  };

  return (
    <generalContext.Provider value={{ mobile, colorScheme, setColorType }}>
      {children}
    </generalContext.Provider>
  );
};

export const mobileResponsive = () => {
  const { mobile } = React.useContext(generalContext);
  return mobile;
};

export const themeColorState = (): [
  ColorScheme,
  (colorType: ColorType) => void
] => {
  const { colorScheme, setColorType } = React.useContext(generalContext);
  return [colorScheme, setColorType];
};

export const getCurrentThemeColor = (): ColorScheme => {
  const { colorScheme } = React.useContext(generalContext);
  return colorScheme;
};
