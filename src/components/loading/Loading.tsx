import * as React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";
import { PlatformModal } from "src/components/platform-modal/index";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props {
  visible: boolean;
}

export const Loading: React.FC<Props> = ({ visible }) => {
  const themeColor = getCurrentThemeColor();
  const styles = StyleSheet.create({
    content: {
      backgroundColor: themeColor.main,
      padding: 22,
      justifyContent: "center",
      alignItems: "center",
      borderRadius: 4,
      borderColor: "rgba(0, 0, 0, 0.1)",
    },
    modalBackground: {
      flex: 1,
      alignItems: "center",
      flexDirection: "column",
      justifyContent: "space-around",
      backgroundColor: themeColor.bright,
    },
    activityIndicatorWrapper: {
      backgroundColor: themeColor.main,
      height: 100,
      width: 100,
      borderRadius: 10,
      display: "flex",
      alignItems: "center",
      justifyContent: "space-around",
      alignSelf: "center",
    },
  });

  return (
    <PlatformModal isVisible={visible}>
      <View style={styles.activityIndicatorWrapper}>
        <ActivityIndicator animating={visible} />
      </View>
    </PlatformModal>
  );
};
