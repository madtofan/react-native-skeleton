import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { StyledText } from "src/components/styled-text/StyledText";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props extends RouteComponentProps {
  path?: string;
  icon: string;
  textButton?: string;
  onPress?: () => void;
}

const NavigationButtonRouterless: React.FC<Props> = ({
  history,
  path = "",
  icon,
  textButton,
  onPress,
}) => {
  const themeColor = getCurrentThemeColor();
  const styles = StyleSheet.create({
    navButtonView: {
      flexGrow: 1,
      height: "100%",
      justifyContent: "center",
      alignItems: "center",
      paddingTop: 10,
    },
  });

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={styles.navButtonView}
      onPress={
        onPress
          ? onPress
          : () => {
              history.push(path);
            }
      }
    >
      <MaterialIcons
        size={18}
        color={location.pathname === path ? themeColor.bright : themeColor.font}
        name={icon}
      />
      {textButton && (
        <StyledText
          style={[
            {
              color:
                location.pathname === path
                  ? themeColor.bright
                  : themeColor.font,
              justifyContent: "center",
              fontSize: 10,
              paddingBottom: 5,
            },
          ]}
        >
          {textButton}
        </StyledText>
      )}
    </TouchableOpacity>
  );
};

export const NavigationButton = withRouter(NavigationButtonRouterless);
