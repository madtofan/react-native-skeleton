import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { getCurrentThemeColor } from "src/utils/ContextProvider";
import { StyledText } from "../styled-text/StyledText";

interface Props {
  selected: boolean;
  onPress: any;
  style?: any;
  textStyle?: any;
  size?: number;
  color?: string;
  text?: string;
}

const styles = StyleSheet.create({
  checkBox: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export const CheckBox: React.FC<Props> = ({
  selected,
  onPress,
  style,
  textStyle,
  size = 15,
  color,
  text = "",
}) => {
  return (
    <TouchableOpacity style={[styles.checkBox, style]} onPress={onPress}>
      <MaterialIcons
        size={size}
        color={color ? color : getCurrentThemeColor().border}
        name={selected ? "check-box" : "check-box-outline-blank"}
      />

      <StyledText style={textStyle}> {text} </StyledText>
    </TouchableOpacity>
  );
};
