import * as React from "react";
import { useContext } from "react";
import { Redirect, Route } from "src/components/router/index";
import { HOME_PATH, LOGIN_PATH } from "src/constants/route-path";
import { GenericStoreContext } from "src/stores/GenericStore";

interface Props {
  component: any;
  securedContent: boolean;
  path: string;
}

export const ProtectedRoute: React.FC<Props> = ({
  component: Component,
  securedContent,
  path,
}) => {
  const loggedIn = useContext(GenericStoreContext).loginStore.details.loggedIn;
  return (
    <Route
      path={path}
      render={(props) =>
        (loggedIn && securedContent) || (!loggedIn && !securedContent) ? (
          <Component {...props} />
        ) : (
          <Redirect
            push
            to={{
              pathname: securedContent ? LOGIN_PATH : HOME_PATH,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
