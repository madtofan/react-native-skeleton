import * as React from "react";
import { StyleProp, StyleSheet, Text, TextStyle } from "react-native";
import { getCurrentThemeColor } from "src/utils/ContextProvider";

interface Props {
  style?: StyleProp<TextStyle>;
}

export const StyledText: React.FC<Props> = ({ style, children }) => {
  const styles = StyleSheet.create({
    text: { color: getCurrentThemeColor().font },
  });

  return <Text style={[styles.text, style]}>{children}</Text>;
};
