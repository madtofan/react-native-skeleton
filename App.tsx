import * as Font from "expo-font";
import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { Loading } from "src/components/loading/Loading";
import { Routes } from "src/routes/Routes";
import { GenericStoreContext } from "src/stores/GenericStore";
import {
  ContextProvider,
  getCurrentThemeColor
} from "src/utils/ContextProvider";

interface Props { }

const drawerStyles = {
  drawer: { shadowColor: "black", shadowOpacity: 0.8, shadowRadius: 3 },
  main: { paddingLeft: 3 },
};

const styles = StyleSheet.create({
  main: {
    width: "100%",
    height: "100%",
  },
});

const App: React.FC<Props> = observer(() => {
  const store = React.useContext(GenericStoreContext);
  const [loading, setloading] = useState(true);
  const firstRender = React.useRef(true);
  const themeColor = getCurrentThemeColor();

  useEffect(() => {
    async function loadFont() {
      await Font.loadAsync({
        Montserrat: require("src/others/font/Montserrat-Medium.otf"),
        MontserratBold: require("src/others/font/Montserrat-Bold.otf"),
        MontserratThin: require("src/others/font/Montserrat-Thin.otf"),
        MontserratLight: require("src/others/font/Montserrat-Light.otf"),
        MontserratSemiBold: require("src/others/font/Montserrat-SemiBold.otf"),
      });
      setloading(false);
    }
    if (firstRender.current) {
      firstRender.current = false;
      loadFont();
      const customTextProps = {
        style: {
          fontFamily: "Montserrat",
          color: themeColor.font,
        },
      };
      // setCustomText(customTextProps);
    }
  });

  if (loading) {
    return <View />;
  } else {
    return (
      <ContextProvider>
        <Loading visible={store.loadingStore.loading} />
        <View
          style={[
            {
              paddingTop: Platform.OS === "web" ? 0 : StatusBar.currentHeight,
              height: "100%",
              width: "100%",
            },
          ]}
        >
          <Routes />
        </View>
      </ContextProvider>
    );
  }
});

export default App;
